FactoryGirl.define do
  factory :page do
    title {Faker::Lorem.sentence(3, false, 0)}
    body {Faker::Lorem.paragraph(7)}
  end
end
