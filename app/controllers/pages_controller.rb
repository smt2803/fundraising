class PagesController < ApplicationController

  responders :flash
  respond_to :html

  before_action :set_page, only: [:show]
  
  #GET /page/:id
  def show
    respond_with(@page)
  end
  
  def main
  end	
  
  private
  
  #set current page
  def set_page
    Page.find_by_slug(params[:id])
  end
end