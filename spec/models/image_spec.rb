require 'rails_helper'

RSpec.describe Image, type: :model do
  it { should validate_length_of(:alt).is_at_most(255) }
  it { should have_attached_file(:file) }
  it { should validate_attachment_presence(:file) }
  it { should validate_attachment_content_type(:file).
                allowing('image/png', 'image/gif').
                rejecting('text/plain', 'text/xml') }
  it { should validate_attachment_size(:file).
                in(0..3.megabytes) }
end
