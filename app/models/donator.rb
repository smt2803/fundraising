class Donator < User
  has_many :donations

  validates :first_name, :last_name, presence: true
end
