FactoryGirl.define do
  factory :country do
    name Faker::Lorem.words(3).join(" ")
  end
end
