class Country < ActiveRecord::Base
  has_many :state
  has_many :address

  validates :name, presence: true
end
