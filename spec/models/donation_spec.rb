require 'rails_helper'

RSpec.describe Donation, type: :model do
  it { should validate_presence_of :description }
  it { should validate_length_of(:description).is_at_least(50) }
end
