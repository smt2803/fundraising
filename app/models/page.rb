class Page < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  validates :title, :body, presence: true
  validates :body, length: {maximum: 65535}
  validates :title, length: {maximum: 255}
end
