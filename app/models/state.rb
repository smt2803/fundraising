class State < ActiveRecord::Base
  belongs_to  :country
  validates :name, :abbreviation, presence: true
end
