class Address < ActiveRecord::Base
  belongs_to  :country
  belongs_to  :state

  validates :zipcode, :address_line_1, :address_line_2,  presence: true
end
