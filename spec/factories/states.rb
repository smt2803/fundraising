FactoryGirl.define do
  factory :state do
    name Faker::Lorem.words(3).join(" ")
    abbreviation Faker::Address.state_abbr
  end
end