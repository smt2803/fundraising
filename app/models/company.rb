class Company < ActiveRecord::Base
  
  has_many :donations
  has_and_belongs_to_many :categories
  has_many :images, as: :imageable, dependent: :destroy
  has_many :videos, as: :videoable, dependent: :destroy
  

  validates :title, :description, :started_at, :finished_at, presence: true
  validates :title, length: { maximum: 255 }
  validates :description, length: { minimum: 50 }
end
