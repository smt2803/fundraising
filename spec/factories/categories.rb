FactoryGirl.define do
  factory :category do
    name Faker::Lorem.words(3).join(" ")
    slug Faker::Internet.slug
  end
end
