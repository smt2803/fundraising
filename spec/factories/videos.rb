FactoryGirl.define do
  factory :video do
    description { Faker::Lorem.sentence(3) }
    url 'https://www.youtube.com/embed/1zpf8H_Dd40?autoplay=1'
  end
end
