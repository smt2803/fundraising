require 'rails_helper'

RSpec.describe Video, type: :model do
  it { should validate_length_of(:description).is_at_most(255) }
  it { should validate_attachment_size(:file).
                in(0..100.megabytes) }
  it { should have_attached_file(:file) }
  it { should validate_presence_of :description }
end
