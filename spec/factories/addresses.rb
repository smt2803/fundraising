FactoryGirl.define do
  factory :address do
    zipcode { Faker::Address.zip_code }
    address_line_1 { Faker::Address.street_name }
    address_line_2 { Faker::Address.building_number }
    state_id { Faker::Number.between(1, 10) }
    country_id { Faker::Number.between(1, 10) }
  end
end
