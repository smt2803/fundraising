class AddAmountCentsToDonations < ActiveRecord::Migration
  def change
  	add_monetize :donations, :amount_cents 
  end
end
