class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string "zipcode"
      t.integer "countri_id"
      t.integer "state_id"
      t.string "address_line_1"
      t.string "address_line_2"
      t.timestamps null: false
    end
  end
end
