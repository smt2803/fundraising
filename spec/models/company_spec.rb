require 'rails_helper'

RSpec.describe Company, type: :model do
  it { should validate_presence_of :title }
  it { should validate_presence_of :description }
  it { should validate_presence_of :started_at }
  it { should validate_presence_of :finished_at } 
  it { should validate_length_of(:description).is_at_least(50) }
  it { should validate_length_of(:title).is_at_most(255) }
end
