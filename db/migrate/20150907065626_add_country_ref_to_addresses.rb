class AddCountryRefToAddresses < ActiveRecord::Migration
  def change
    remove_column :addresses, :countri_id, :integer
    remove_column :addresses, :state_id, :integer
    add_reference :addresses, :country, index: true, foreign_key: true
    add_reference :addresses, :state, index: true, foreign_key: true
  end
end
