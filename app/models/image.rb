class Image < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true

  has_attached_file :file, styles: { medium: "300x300>", thumb: "100x100>" },
    default_url: "/images/:style/missing.png"

  validates_attachment_content_type :file, :content_type => /\Aimage\/.*\Z/

  validates_attachment :file, presence: true, size: { in: 0..3.megabytes }

  validates(
    :alt,
    length: { maximum: 255 }
  )
end
