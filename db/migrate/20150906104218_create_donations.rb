class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.text :description
      t.boolean :anonymoys, default: false

      t.timestamps null: false
    end
  end
end
