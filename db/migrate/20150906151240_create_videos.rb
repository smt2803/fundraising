class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.attachment :file
      t.string :description
      t.references :videoable, polymorphic: true, index: true
      t.text :url
      t.timestamps null: false
    end
  end
end
