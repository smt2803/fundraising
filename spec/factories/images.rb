FactoryGirl.define do
  factory :image do
    file { Faker::Avatar.image("my-own-slug")  }
    alt { Faker::Lorem.word }
  end

end
