FactoryGirl.define do
  sequence :email do |n|
    "user#{n}@test.com"
  end
  
  factory :recipient, class: 'Recipient' do
    email { Faker::Internet.email }    
    password '12345678'
    password_confirmation '12345678'
  end
end
