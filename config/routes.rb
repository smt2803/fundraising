Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  devise_for :users
  root "pages#main"
  resources :pages, only: [:show]
end
