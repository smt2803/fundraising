FactoryGirl.define do
  factory :donator, class: 'Donator' do
    first_name Faker::Name.name
    last_name Faker::Name.name
  end
end
