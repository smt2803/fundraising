class Donation < ActiveRecord::Base
  belongs_to :donator, inverse_of: :donation
  belongs_to :company, inverse_of: :donation

  validates :description, presence: true
  validates :description, length: { minimum: 50 }

  monetize :amount_cents
end
