require 'rails_helper'

RSpec.describe PagesController, type: :controller do
  describe "GET #show" do
    let(:page) { FactoryGirl.create(:page) }
    
    it "returns http success" do
      get :show, id: page.id
      expect(response).to have_http_status(:success)
    end
  end
end
