class Video < ActiveRecord::Base
  belongs_to :videoable, polymorphic: true

  has_attached_file :file
  validates_attachment_content_type :file, :content_type => /\Avideo\/.*\Z/
  validates_attachment :file, size: { in: 0..100.megabytes }

  validates(
    :description,
    length: { maximum: 255 }, presence: true
  )
end
