FactoryGirl.define do
  factory :company do
    title { Faker::Lorem.characters(20) }
    description { Faker::Lorem.characters(60) }
    started_at { Faker::Date.between(2.days.ago, Date.today) }
    finished_at { Faker::Date.forward(23) }
  end
end
