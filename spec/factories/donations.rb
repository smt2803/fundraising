FactoryGirl.define do
  factory :donation do
    description { Faker::Lorem.characters(60) }
    anonymoys false
  end
end
