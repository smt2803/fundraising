class Category < ActiveRecord::Base
  validates :name, presence: true
  extend FriendlyId
  friendly_id :name
end
