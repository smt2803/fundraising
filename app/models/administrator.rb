class Administrator < User
  def admin?
    self.class == Administrator
  end
end