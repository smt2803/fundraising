class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string   "title"
      t.text "description"
      t.string "slug"
      t.datetime "started_at"
      t.datetime "finished_at"
      t.timestamps null: false
    end
  end
end
